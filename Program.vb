﻿Module Program

    Public Sub Main()

        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)

        Dim returnString As String ' = ""
        Dim token As String ' = ""
        Dim lotId As String ' = ""

#If DEBUG Then
        'Dim exeFullPathName As String = Application.ExecutablePath
        Dim exeFullPathName As String = "C:\\VersionControlHubVBSample-32bit\\VersionControlHubVBSample.exe"
        Dim repoPath As String = System.IO.Path.GetDirectoryName(exeFullPathName) ' + "\\"

        VersionControl.NET.Dll.CheckProgramVersion(
            exeFullPathName,
            repoPath,
            returnString,
            token,
            lotId)
#Else
        Dim exeFullPathName As String = Application.ExecutablePath
        'Dim exeFullPathName As String = "C:\\VersionControlHubVBSample-32bit\\VersionControlHubVBSample.exe"
        Dim repoPath As String = System.IO.Path.GetDirectoryName(exeFullPathName) ' + "\\"

        VersionControl.NET.Dll.CheckProgramVersion(
            exeFullPathName,
            repoPath,
            returnString,
            token,
            lotId)
#End If

        Application.Run(New Form1)

    End Sub

End Module
