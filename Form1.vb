﻿Public Class Form1

    Dim timer_DelayBeforeAutomaticMachineVersionCheck As System.Windows.Forms.Timer

    Private Sub openLot_Button_Click(sender As Object, e As EventArgs) Handles openLot_Button.Click

        Dim returnString As String ' = ""
        Dim token As String = comboBoxToken.Text
        Dim lotId As String = comboBoxLotId.Text

#If DEBUG Then
        'Dim exeFullPathName As String = Application.ExecutablePath
        Dim exeFullPathName As String = "C:\\VersionControlHubVBSample-32bit\\VersionControlHubVBSample.exe"
        Dim repoPath As String = System.IO.Path.GetDirectoryName(exeFullPathName) ' + "\\"

        VersionControl.NET.Dll.CheckProgramVersion(
            exeFullPathName,
            repoPath,
            returnString,
            token,
            lotId)
#Else
        Dim exeFullPathName As String = Application.ExecutablePath
        'Dim exeFullPathName As String = "C:\\VersionControlHubVBSample-32bit\\VersionControlHubVBSample.exe"
        Dim repoPath As String = System.IO.Path.GetDirectoryName(exeFullPathName) ' + "\\"

        VersionControl.NET.Dll.CheckProgramVersion(
            exeFullPathName,
            repoPath,
            returnString,
            token,
            lotId)
#End If

        comboBoxToken.Text = token

        'if (timer_DelayBeforeAutomaticMachineVersionCheck.Enabled)
        Dim i As Int32 = 0
        Dim delay As String = comboBoxDelay.Text
        Dim IsNumeric As Boolean = Int32.TryParse(delay, i)

        If comboBoxDelay.Text <> "0" And comboBoxDelay.Text <> "" And IsNumeric Then
            'timer_DelayBeforeAutomaticMachineVersionCheck = New System.Windows.Forms.Timer();
            'timer_DelayBeforeAutomaticMachineVersionCheck.Tick += New EventHandler(timer_DelayBeforeAutomaticMachineVersionCheck_tick);
            timer_DelayBeforeAutomaticMachineVersionCheck.Interval = Convert.ToInt32(comboBoxDelay.Text)
            timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = True
            timer_DelayBeforeAutomaticMachineVersionCheck.Start()
        End If

    End Sub

    Private Sub buttonMachineVersionStatusManualCheck_Click(sender As Object, e As EventArgs) Handles buttonMachineVersionStatusManualCheck.Click

        Dim machineOpenLotFailedVersionCheckInfo As VersionControl.NET.Dll.MachineOpenLotVersionCheckInfo =
            VersionControl.NET.Dll.CheckMachineVersion(
                comboBoxLotId.Text,
                comboBoxToken.Text)

        If (machineOpenLotFailedVersionCheckInfo.machineProceedWithOpenLot) Then
            MessageBox.Show("Machine version check PASS")
        Else
            Dim failureDetails As String = ""

            For i As Integer = 0 To machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos.Length
                failureDetails &=
                    machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos(i).ipAddress &
                    ": " & machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos(i).programType &
                    ": " & machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos(i).failureInfo &
                    "\n\n"
            Next

            MessageBox.Show(String.Format(
                    "Machine version check FAILED:\n\n{0}",
                    failureDetails))
        End If

    End Sub

    Private Sub buttonGetLocalIPAddress_Click(sender As Object, e As EventArgs) Handles buttonGetLocalIPAddress.Click

        Dim localIPAddress As String = VersionControl.NET.Dll.GetLocalIPAddress()

        MessageBox.Show(localIPAddress)

    End Sub

    Private Sub comboBoxLotId_MouseClick(sender As Object, e As MouseEventArgs) Handles comboBoxLotId.MouseClick

        Dim i As Int32 = 0

        Dim s As String = comboBoxLotId.Text

        Dim result As Boolean = Integer.TryParse(s, i)

        If (result = True) Then
            comboBoxLotId.Text = Convert.ToString(i + 1)
        End If

    End Sub

    Private Sub comboBoxDelay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles comboBoxDelay.SelectedIndexChanged

        If comboBoxDelay.Text = "0" Or comboBoxDelay.Text = "" Then

            buttonMachineVersionStatusManualCheck.Enabled = True

            'timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = false;
        Else
            buttonMachineVersionStatusManualCheck.Enabled = False

            'timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = true;
        End If

    End Sub
End Class
